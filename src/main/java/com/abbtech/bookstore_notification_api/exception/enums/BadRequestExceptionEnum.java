package com.abbtech.bookstore_notification_api.exception.enums;

import lombok.Getter;

@Getter
public enum BadRequestExceptionEnum {
    GENERAL_BUSINESS_ERROR("PROJECT-BIZ-0001", 400),
    USER_NOT_FOUND("PROJECT-BIZ-0002", 400),
    DEFINITION_NOT_ACCEPTABLE("PROJECT-BIZ-0003", 422),
    ORDER_NOT_FOUND("PROJECT-BIZ-0004", 400);

    private final String errorCode;
    private final int statusCode;

    BadRequestExceptionEnum(String errorCode, int statusCode) {
        this.errorCode = errorCode;
        this.statusCode = statusCode;
    }
}
