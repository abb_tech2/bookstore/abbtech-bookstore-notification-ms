package com.abbtech.bookstore_notification_api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationReqDTO {
    private UUID orderId;
    private String userEmail;
    private UUID userId;
    private String message;
}
