package com.abbtech.bookstore_notification_api.dto;

import java.util.UUID;

public record NotificationRespDTO(
        UUID id,
        String message,
        String userEmail,
        UUID userId,
        UUID orderId) {
}
