package com.abbtech.bookstore_notification_api.repository;

import com.abbtech.bookstore_notification_api.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface NotificationRepository extends JpaRepository<Notification, UUID> {

}
