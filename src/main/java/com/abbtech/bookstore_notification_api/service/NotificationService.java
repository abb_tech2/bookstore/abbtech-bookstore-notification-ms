package com.abbtech.bookstore_notification_api.service;

import com.abbtech.bookstore_notification_api.dto.NotificationReqDTO;
import com.abbtech.bookstore_notification_api.dto.NotificationRespDTO;

import java.util.List;

public interface NotificationService {
    void send(NotificationReqDTO notification);

    List<NotificationRespDTO> getAll();
}
