package com.abbtech.bookstore_notification_api.service.impl;

import com.abbtech.bookstore_notification_api.dto.NotificationReqDTO;
import com.abbtech.bookstore_notification_api.dto.NotificationRespDTO;
import com.abbtech.bookstore_notification_api.model.Notification;
import com.abbtech.bookstore_notification_api.repository.NotificationRepository;
import com.abbtech.bookstore_notification_api.service.NotificationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRepository;

    @Override
    @KafkaListener(topics = "order-confirmation", groupId = "my-group")
    public void send(NotificationReqDTO notificationReqDTO) {
        log.info("Sending notification: {}", notificationReqDTO);

        Notification notification = new Notification();
        notification.setMessage("Your order has been confirmed!");
        notification.setUserEmail(notificationReqDTO.getUserEmail());
        notification.setUserId(notificationReqDTO.getUserId());
        notification.setOrderId(notificationReqDTO.getOrderId());

        notificationRepository.save(notification);

    }

    @Override
    public List<NotificationRespDTO> getAll() {
        log.info("Getting all notifications");
        var notifications = notificationRepository.findAll();
        return notifications.stream()
                .map(notification -> new NotificationRespDTO(notification.getId(), notification.getMessage(),
                        notification.getUserEmail(), notification.getUserId(), notification.getOrderId())).toList();
    }
}
