package com.abbtech.bookstore_notification_api.controller;

import com.abbtech.bookstore_notification_api.dto.NotificationReqDTO;
import com.abbtech.bookstore_notification_api.dto.NotificationRespDTO;
import com.abbtech.bookstore_notification_api.exception.ErrorDetailDTO;
import com.abbtech.bookstore_notification_api.service.NotificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Validated
@RestController
@RequestMapping("/notifications")
@RequiredArgsConstructor
@Tag(name = "Notification Controller", description = "APIs for managing notifications including sending and retrieving notifications.")
public class NotificationController {
    private final NotificationService notificationService;

    @Operation(summary = "Send a notification", description = "Send a new notification with the provided details.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Notification successfully sent"),
            @ApiResponse(responseCode = "400", description = "Invalid request data",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void send(@Valid @RequestBody NotificationReqDTO notificationRequestDto) {
        notificationService.send(notificationRequestDto);
    }

    @Operation(summary = "Get all notifications", description = "Retrieve a list of all notifications.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully retrieved list of notifications"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content(schema = @Schema(implementation = ErrorDetailDTO.class)))
    })
    @GetMapping
    public List<NotificationRespDTO> getAll() {
        return notificationService.getAll();
    }
}
